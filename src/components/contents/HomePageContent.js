import { Container, Grid, Typography } from "@mui/material"

const HomePageContent = () => {
    return (
        <Container>
            <Grid container>
                <Grid item>
                    <Typography>Home Page Content</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default HomePageContent