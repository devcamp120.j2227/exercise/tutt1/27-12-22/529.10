import { Container, Grid, Typography } from "@mui/material"

const ThirdPageContent = () => {
    return (
        <Container>
            <Grid container>
                <Grid item>
                    <Typography>Third Page Content</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default ThirdPageContent