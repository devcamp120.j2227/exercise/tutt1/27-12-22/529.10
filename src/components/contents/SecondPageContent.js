import { Container, Grid, Typography } from "@mui/material"
import { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom"

const SecondPageContent = () => {
    const { param1, param2 } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        if(param1 === "third") {
            navigate("/thirdpage")
        }
    })
    return (
        <Container>
            <Grid container>
                <Grid item>
                    <Typography>Second Page Content: param1 = {param1}, param2 = {param2}</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default SecondPageContent