import { Container, Grid, Typography } from "@mui/material"

const FirstPageContent = () => {
    return (
        <Container>
            <Grid container>
                <Grid item>
                    <Typography>First Page Content</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default FirstPageContent