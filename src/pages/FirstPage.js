import FirstPageContent from "../components/contents/FirstPageContent"
import Header from "../components/Header"

const FirstPage = () => {
    return (
        <>
            <Header/>
            <FirstPageContent/>
        </>
    )
}

export default FirstPage