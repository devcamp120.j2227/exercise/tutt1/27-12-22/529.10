
import HomePageContent from "../components/contents/HomePageContent";
import Header from "../components/Header";

const Home = () => {
    return (
        <>
            <Header/>
            <HomePageContent/> 
        </>
    )
}

export default Home