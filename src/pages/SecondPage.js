import SecondPageContent from "../components/contents/SecondPageContent"
import Header from "../components/Header"

const SecondPage = () => {
    return (
        <>
            <Header/>
            <SecondPageContent/>
        </>
    )
}

export default SecondPage