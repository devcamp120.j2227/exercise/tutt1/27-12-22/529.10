import ThirdPageContent from "../components/contents/ThirdPageContent"
import Header from "../components/Header"

const ThirdPage = () => {
    return (
        <>
            <Header/>
            <ThirdPageContent/>
        </>
    )
}

export default ThirdPage